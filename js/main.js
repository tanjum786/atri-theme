$(document).ready(function(){
    
    $('.headMenu .hs-item-has-children > a').after(' <div class="childTrigger"></div>');
    
    $('.menuTrigger').click(function() {
        $('body').toggleClass('mobile-open');
        $('.childTrigger').removeClass('child-open');
        $('.headRight').slideToggle(250);
        return false;
     });

    $('.childTrigger').click(function() {
        $(this).parent().siblings('.hs-item-has-children').find('.childTrigger').removeClass('child-open');
        $(this).parent().siblings('.hs-item-has-children').find('.hs-menu-children-wrapper').slideUp(250)
        $(this).next('.hs-menu-children-wrapper').slideToggle(250);
        $(this).next('.hs-menu-children-wrapper').children('.hs-item-has-children').find('.hs-menu-children-wrapper').slideUp(250);
        $(this).next('.hs-menu-children-wrapper').children('.hs-item-has-children').find('.child-trigger').removeClass('child-open');
        $(this).toggleClass('child-open');
        return false;
    });
  






  equalheight = function(container){

  var currentTallest = 0,
      currentRowStart = 0,
      rowDivs = new Array(),
      $el,
      topPosition = 0;
  $(container).each(function() {

    $el = $(this);
    $($el).height('auto')
    topPostion = $el.position().top;

    if (currentRowStart != topPostion) {
      for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
        rowDivs[currentDiv].height(currentTallest);
      }
      rowDivs.length = 0; // empty the array
      currentRowStart = topPostion;
      currentTallest = $el.height();
      rowDivs.push($el);
    } else {
      rowDivs.push($el);
      currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
   }
    for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
      rowDivs[currentDiv].height(currentTallest);
    }
  });
  }

  $(window).load(function() {
   equalheight('.blogListing.blogListTop .blogItem');
  });


  $(window).resize(function(){
   equalheight('.blogListing.blogListTop .blogItem');
  });
  
  
  });

  