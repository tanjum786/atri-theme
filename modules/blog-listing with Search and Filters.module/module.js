// document.addEventListener("DOMContentLoaded", function () {
//   let dropDownIcon = document.querySelector(".dropdown-icon img");
//   let dropDownIconTopic = document.querySelector(".dropdown-icon-topic img");
//   const filterButtons1 = document.querySelectorAll(
//     ".blog-topic-filter .select-dropdown__list-item"
//   );
//   const noResultContainer = document.querySelector(".no-result-container");
//   let searchResultInput = document.querySelector(".search-input");
//   let searchIcon = document.querySelector(".search-icon");
//   let searchInput = document.getElementById("searchInput");

  
//   function getTagFromURLPath() {
//     var url = window.location.href;
//     var parts = url.split("/");
//     var tagIndex = parts.indexOf("tag");
//     if (tagIndex !== -1 && tagIndex < parts.length - 1) {
//       return parts[tagIndex + 1];
//     }
//     return "";
//   }

//   // Update the button text based on tag from URL path
//   var urlSelectedTag = getTagFromURLPath();

//   if (urlSelectedTag) {

//     document.getElementById("selectedTagButton").querySelector("span").innerText = urlSelectedTag;
//     filterButtons1.forEach((button) => {
//       let selectedTag = button.getAttribute("data-value").toLowerCase().replace(/\s+/g, '-');
//       if (selectedTag === urlSelectedTag) {
//         button.classList.add("selected");
//       } else {
//         button.classList.remove("selected");

//       }
//     });
//   }

//   filterButtons1.forEach((button) => {
//     button.addEventListener("click", function () {
//       const clickedTag = this.getAttribute("data-value").toLowerCase().replace(/\s+/g, '-');

//       if (this.classList.contains("selected")) {
//         window.location.href = "https://hubspot-demo-account-niswey-7222284.hs-sites.com/atri-theme/";
//         this.classList.remove("selected");
//       } else {
//         window.location.href = "https://hubspot-demo-account-niswey-7222284.hs-sites.com/atri-theme/tag/" + clickedTag;
//         filterButtons1.forEach((otherButton) => {
//           if (otherButton !== this && otherButton.classList.contains("selected")) {
//             otherButton.classList.remove("selected");
//           }
//         });
//         this.classList.add("selected");
//       }
//     });
//   });



//   // Dropdown 1
//   const button1 = document.querySelector(
//     ".blog-topic-filter .select-dropdown__button"
//   );
//   const list1 = document.querySelector(
//     ".blog-topic-filter .select-dropdown__list"
//   );
//   function toggleDropdownTopic() {
//     if (list1.classList.contains("active")) {
//       list1.classList.remove("active");
//       button1.classList.remove("active-btn");
//       dropDownIconTopic.classList.remove("rotate-icon");
//       dropDownIconTopic.src =
//         "https://5887777.fs1.hubspotusercontent-na1.net/hubfs/5887777/Icon.svg";
//     } else {
//       list1.classList.add("active");
//       button1.classList.add("active-btn");
//       dropDownIconTopic.classList.add("rotate-icon");
//       dropDownIconTopic.src =
//         "https://7222284.fs1.hubspotusercontent-na1.net/hubfs/7222284/chevron-down.svg";
//     }
//   }

//   button1.addEventListener("click", function () {
//     toggleDropdownTopic();

//   });

//   const blogs = document.querySelectorAll(".blog-list_wrapper");

//   function filterContentByTagAndType(tag, type) {
//     let anyBlogsDisplayed = false;
//     blogs.forEach(function (blog) {
//       const tagsString = blog.getAttribute("data-tags");
//       const tags = tagsString.split(",");
//       const blogType = blog
//         .querySelector(".blog-type-wrapper")
//         .textContent.trim()
//         .toLowerCase();
//       if (
//         (tag === "All" || tags.includes(tag)) &&
//         (type === "all" || blogType === type)
//       ) {
//         blog.style.display = "block";
//         anyBlogsDisplayed = true;
//       } else {
//         blog.style.display = "none";
//       }
//     });
//     if (anyBlogsDisplayed) {
//       noResultContainer.style.display = "none";
//     } else {
//       noResultContainer.style.display = "flex";
//       const noResultSpan = document.querySelector(".no-result");
//       noResultSpan.textContent = `Oops! Looks like there are no posts related to '${tag == "All" ? "" : tag.toLowerCase()}' ${type == "all" ? "" : "and " + type}`;
//     }

//     if (searchInput.value.length > 0) {

//       filterSearchResults(tag, type);
//     }
//   }

//   // Dropdown 2
//   const button2 = document.querySelector(
//     ".blog-type-filter .select-dropdown__button"
//   );
//   const list2 = document.querySelector(
//     ".blog-type-filter .select-dropdown__list"
//   );
//   const blogs2 = document.querySelectorAll(".blog-list_wrapper");

//   function toggleDropdown() {
//     if (list2.classList.contains("active")) {
//       list2.classList.remove("active");
//       button2.classList.remove("active-btn");
//       dropDownIcon.classList.remove("rotate-icon");
//       dropDownIcon.src =
//         "https://5887777.fs1.hubspotusercontent-na1.net/hubfs/5887777/Icon.svg";
//     } else {
//       list2.classList.add("active");
//       button2.classList.add("active-btn");
//       dropDownIcon.classList.add("rotate-icon");
//       dropDownIcon.src =
//         "https://7222284.fs1.hubspotusercontent-na1.net/hubfs/7222284/chevron-down.svg";
//     }
//   }

//   button2.addEventListener("click", function () {
//     toggleDropdown();

//   });

//   function filterContentByTag2(tag) {
//     blogs2.forEach(function (blog) {
//       const blogType = blog
//         .querySelector(".blog-type-wrapper")
//         .textContent.trim()
//         .toLowerCase();
//       if (tag === "all" || blogType === tag) {
//         blog.style.display = "block";
//       } else {
//         blog.style.display = "none";
//       }
//     });
//   }

//   const filterButtons2 = document.querySelectorAll(
//     ".blog-type-filter .select-dropdown__list-item"
//   );
//   filterButtons2.forEach((button) => {
//     button.addEventListener("click", function () {
//       let selectedValue = this.textContent.trim().toLowerCase();
//       let selectedTag = button1.getAttribute("data-value") || "All";

//       // Check if the clicked button is already selected
//       if (this.classList.contains("selected")) {
//         // If the same filter is clicked again, clear the filter
//         filterButtons2.forEach((btn) => {
//           btn.classList.remove("selected");
//         });
//         selectedValue = "all"; // Reset to show all items
//         button2.querySelector("span").textContent = "Resource Type";
//       } else {
//         // Otherwise, apply the filter
//         filterButtons2.forEach((btn) => {
//           btn.classList.remove("selected");
//         });
//         this.classList.add("selected");
//         button2.querySelector("span").textContent = selectedValue === "all" ? "Resource Type" : selectedValue;
//       }

//       // Apply the filter based on the selected tag and type
//       filterContentByTag2(selectedValue);
//       filterContentByTagAndType(selectedTag, selectedValue);

//       // Update the selected value attribute and text content of the dropdown button
//       button2.setAttribute("data-value", selectedValue);
//       list2.classList.remove("active");
//       dropDownIcon.classList.remove("rotate-icon");
//     });
//   });

//   document.addEventListener("DOMContentLoaded", function () {
//     var selectedTag = getUrlParameter("tag");
//     updateFilteredButton(selectedTag);
//   });


//   // Close dropdowns when clicking outside
//   document.addEventListener("click", function (event) {
//     const dropdownButtons = document.querySelectorAll(
//       ".select-dropdown__button"
//     );
//     dropdownButtons.forEach((button) => {
//       if (!button.contains(event.target)) {
//         const dropdownList = button.nextElementSibling;
//         dropdownList.classList.remove("active");
//         const dropdownIcon = button.querySelector(".dropdown-icon img");
//         let dropDownIconTopic = button.querySelector(
//           ".dropdown-icon-topic img"
//         );

//         if (dropdownIcon) {
//           dropdownIcon.classList.remove("rotate-icon");
//         }
//         if (dropDownIconTopic) {
//           dropDownIconTopic.classList.remove("rotate-icon");
//         }
//       }
//     });
//   });

//   // mobile view search  and filter functionality **************************************************
//   let modal = document.querySelector(".search-modal");
//   let searchBtn = document.querySelector(".modal-searchBtn");
//   let closeBtn = document.querySelector(".close-icon");
//   let filterModal = document.querySelector(".filter-modal");
//   let filterBtn = document.querySelector(".modal-filterBtn");
//   let filterCloseBtn = document.querySelector(".filter-close-icon");
//   let selectedTopicItems = document.querySelectorAll(".blog-modal_topic-list .modal-select-dropdown__list-item");
//   let selectedTypeItems = document.querySelectorAll(".blog-modal_type-list .modal-select-dropdown__list-item-type");
//   const mobileFilterBtn = document.querySelector(".modal-filter_btn");
//   let mobileSearchInput = document.getElementById("mobile-search-input");
//   let MobilesearchIcon = document.querySelector(".modal-search-icon ");
//   let mobileSearchResultBtn = document.querySelector(".modal-search_btn");
//   let errorMsg = document.querySelector(".error-msg");

//   function toggleSearchModal() {
//     modal.classList.toggle("modal-toggle");
//     document.body.classList.toggle("modal-open"); // Add or remove class to body

//   }

//   mobileSearchInput.addEventListener("input", function () {
//     if (mobileSearchInput.value.length > 0) {
//       errorMsg.style.display = "none";
//       MobilesearchIcon.src = "https://7222284.fs1.hubspotusercontent-na1.net/hubfs/7222284/x-circle.svg";
//     } else {
//       document.getElementById("mobile-searchContainer").classList.remove("focused");
//       MobilesearchIcon.src = "https://5887777.fs1.hubspotusercontent-na1.net/hubfs/5887777/search.svg";
//     }


//   });

//   mobileSearchResultBtn.addEventListener("click", function () {
//     if (mobileSearchInput.value.length > 0) {
//       filterSearchResultsMobile(mobileSearchInput.value.trim().toLowerCase());
//       toggleSearchModal();
//       searchBtn.classList.add("btn-bg");


//     } else {
//       errorMsg.style.display = "block";

//     }
//   });

//   // Event listeners for opening and closing search modal
//   searchBtn.addEventListener("click", toggleSearchModal);
//   closeBtn.addEventListener("click", function () {
//     toggleSearchModal();
//     mobileSearchInput.value = "";
//     document.getElementById("mobile-searchContainer").classList.remove("focused");
//     MobilesearchIcon.src = "https://5887777.fs1.hubspotusercontent-na1.net/hubfs/5887777/search.svg";
//     errorMsg.style.display = "none";

//   });


//   // Function to toggle filter modal
//   function toggleFilterModal() {
//     filterModal.classList.toggle("filter-modal_toggle");
//     document.body.classList.toggle("modal-open"); // Add or remove class to body

//   }

//   // Event listeners for opening and closing filter modal
//   filterBtn.addEventListener("click", toggleFilterModal);
//   filterCloseBtn.addEventListener("click", toggleFilterModal);

//   // Function to apply filters
//   function applyFilters(selectedTag, selectedType) {
//     const blogs = document.querySelectorAll(".blog-list_wrapper");
//     let anyBlogsDisplayedMobile = false;

//     blogs.forEach(function (blog) {
//       const tagsString = blog.getAttribute("data-tags");
//       const tags = tagsString.split(",");
//       const blogType = blog.querySelector(".blog-type-wrapper").textContent.trim().toLowerCase();

//       const tagMatch = selectedTag === "All" || tags.includes(selectedTag);
//       const typeMatch = selectedType === "All" || blogType === selectedType;

//       if (selectedTag === "All" && selectedType === "All") {
//         filterBtn.classList.remove("btn-bg");
//       } else {

//         filterBtn.classList.add("btn-bg");
//       }

//       if (tagMatch && typeMatch) {
//         blog.style.display = "block"; // Show the blog
//         anyBlogsDisplayedMobile = true;

//       } else {
//         blog.style.display = "none"; // Hide the blog
//       }
//     });

//     if (anyBlogsDisplayedMobile) {
//       noResultContainer.style.display = "none";
//     } else {
//       noResultContainer.style.display = "flex";
//       const noResultSpan = document.querySelector(".no-result");
//       noResultSpan.textContent = `Oops! Looks like there are no posts related to '${selectedTag == "All" ? "" : selectedTag.toLowerCase()}' ${selectedType == "All" ? "" : "and " + selectedType}`;
//     }
//     filterSearchResultsMobile(mobileSearchInput.value.trim().toLowerCase());

//   }

//   // Event listener for applying filters
//   mobileFilterBtn.addEventListener("click", function () {
//     // Retrieve selected tag and type
//     let selectedTag = getSelectedTag();
//     let selectedType = getSelectedType();
//     if (selectedTag === "All" && selectedType === "All") {
//       filterBtn.classList.remove("btn-bg");
//     }

//     applyFilters(selectedTag, selectedType);

//     toggleFilterModal();
//   });

//   // Event listener for selecting topic or type
//   [...selectedTopicItems, ...selectedTypeItems].forEach((button) => {
//     button.addEventListener("click", function () {
//       let selectedTag = getSelectedTag();
//       let selectedType = getSelectedType();

//       // Determine the list of items based on the parent element
//       const itemList = button.parentElement.classList.contains("blog-modal_topic-list") ? selectedTopicItems : selectedTypeItems;

//       // If the clicked item is already selected, deselect it and remove the filter
//       if (button.classList.contains("mobile-selected")) {
//         button.classList.remove("mobile-selected");
//         filterModal.classList.toggle("filter-modal_toggle");
//         document.body.classList.remove("modal-open"); // Add or remove class to body


//       } else {
//         // Toggle the selected class for the respective list of items
//         itemList.forEach((btn) => btn.classList.remove("mobile-selected"));
//         button.classList.add("mobile-selected");
//       }

//       // Apply the filter
//       selectedTag = getSelectedTag();
//       selectedType = getSelectedType();
//       applyFilters(selectedTag, selectedType);
//     });
//   });

//   // Function to get the selected topic
//   function getSelectedTag() {
//     let selectedTagElement = document.querySelector(".blog-modal_topic-list .modal-select-dropdown__list-item.mobile-selected");
//     return selectedTagElement ? selectedTagElement.getAttribute("data-value") : "All";
//   }

//   // Function to get the selected type
//   function getSelectedType() {
//     let selectedTypeElement = document.querySelector(".blog-modal_type-list .modal-select-dropdown__list-item-type.mobile-selected");
//     return selectedTypeElement ? selectedTypeElement.textContent.trim().toLowerCase() : "All";
//   };



//   function filterSearchResultsMobile(searchTerm) {
//     const blogs = document.querySelectorAll(".blog-list_wrapper");
//     let anyBlogsDisplayed = false;

//     blogs.forEach(function (blog) {
//       const title = blog.querySelector(".blog-title a").textContent.trim().toLowerCase();
//       const searchMatch = searchTerm === "" || title.includes(searchTerm);

//       // Retrieve selected tag and type
//       let selectedTag = getSelectedTag();
//       let selectedType = getSelectedType();

//       // Apply tag and type filters
//       const tagsString = blog.getAttribute("data-tags");
//       const tags = tagsString.split(",");
//       const blogType = blog.querySelector(".blog-type-wrapper").textContent.trim().toLowerCase();
//       const tagMatch = selectedTag === "All" || tags.includes(selectedTag);
//       const typeMatch = selectedType === "All" || blogType === selectedType;

//       if (searchMatch && tagMatch && typeMatch) {
//         blog.style.display = "block";
//         anyBlogsDisplayed = true;
//       } else {
//         blog.style.display = "none";
//       }
//     });

//     if (searchTerm.length > 0) {
//       searchResult.style.display = "block";
//       searchResultInput.textContent = searchTerm;
//       MobilesearchIcon.src = "https://7222284.fs1.hubspotusercontent-na1.net/hubfs/7222284/x-circle.svg";
//     } else {
//       searchResult.style.display = "none";
//       searchBtn.classList.remove("btn-bg");
//       document.getElementById("mobile-searchContainer").classList.remove("focused");
//       MobilesearchIcon.src = "https://5887777.fs1.hubspotusercontent-na1.net/hubfs/5887777/search.svg";
//     }

//     if (anyBlogsDisplayed) {
//       noResultContainer.style.display = "none";
//     } else if (searchTerm.length > 0) {
//       noResultContainer.style.display = "flex";
//       const noResultSpan = document.querySelector(".no-result");
//       noResultSpan.textContent = `Oops! Looks like there are no posts related to '${searchTerm}' search`;
//     }
//   }

//   MobilesearchIcon.addEventListener("click", function () {
//     if (MobilesearchIcon.src == "https://7222284.fs1.hubspotusercontent-na1.net/hubfs/7222284/x-circle.svg") {
//       mobileSearchInput.value = "";
//       searchTerm = "";
//       searchResult.style.display = "none";
//       noResultContainer.style.display = "none";
//       MobilesearchIcon.src = "https://5887777.fs1.hubspotusercontent-na1.net/hubfs/5887777/search.svg";

//       // Get the current selected tag and type filters+
//       const selectedTag = getSelectedTag();
//       const selectedType = getSelectedType();

//       // Filter blogs based on the selected tag and type
//       applyFilters(selectedTag, selectedType);
//     }
//   });
//   mobileSearchInput.addEventListener("focus", function () {
//     document.getElementById("mobile-searchContainer").classList.add("focused");
//   });


//   // hubsot search

//   var hsSearch = function (_instance) {
//     var TYPEAHEAD_LIMIT = 3;
//     var KEYS = {
//       TAB: 'Tab',
//       ESC: 'Esc', // IE11 & Edge 16 value for Escape
//       ESCAPE: 'Escape',
//       UP: 'Up', // IE11 & Edge 16 value for Arrow Up
//       ARROW_UP: 'ArrowUp',
//       DOWN: 'Down', // IE11 & Edge 16 value for Arrow Down
//       ARROW_DOWN: 'ArrowDown',
//     };
//     var searchTerm = '',
//       searchForm = _instance,
//       searchField = _instance.querySelector('.hs-search-field__input'),
//       searchResults = _instance.querySelector('.hs-search-field__suggestions'),
//       searchIcon = _instance.querySelector('.search-icon'),
//       searchOptions = function () {
//         var formParams = [];
//         var form = _instance.querySelector('form');
//         for (
//           var i = 0;
//           i < form.querySelectorAll('input[type=hidden]').length;
//           i++
//         ) {
//           var e = form.querySelectorAll('input[type=hidden]')[i];
//           if (e.name !== 'limit') {
//             formParams.push(
//               encodeURIComponent(e.name) + '=' + encodeURIComponent(e.value)
//             );
//           }
//         }
//         var queryString = formParams.join('&');
//         return queryString;
//       };
  
//     var debounce = function (func, wait, immediate) {
//       var timeout;searchForm
//       return function () {
//         var context = this,
//           args = arguments;
//         var later = function () {
//           timeout = null;
//           if (!immediate) {
//             func.apply(context, args);
//           }
//         };
//         var callNow = immediate && !timeout;
//         clearTimeout(timeout);
//         timeout = setTimeout(later, wait || 200);
//         if (callNow) {
//           func.apply(context, args);
//         }
//       };
//     },
//       emptySearchResults = function () {
//         searchResults.innerHTML = '';
//         searchField.focus();
//         searchForm.classList.remove('hs-search-field--open');
//       },
//       fillSearchResults = function (response) {
//         var items = [];
//         items.push(
//           "<li id='results-for'>Results for \"" + response.searchTerm + '"</li>'
//         );
//         response.results.forEach(function (val, index) {
//           items.push(
//             "<li id='result" +
//             index +
//             "'><a href='" + 
//             val.url +
//             "'>" +
//             val.title +
//             '</a></li>'
//           );
//           items.push("<li>Post Type: " + val.blog_resource_type + "</li>");
  
//         });
  
//         emptySearchResults();
//         searchResults.innerHTML = items.join('');
//         searchForm.classList.add('hs-search-field--open');
//       },
  
  
//       getSearchResults = function () {
//         if (searchTerm.trim().length === 0) {
//           // Clear the search results if the search term is empty
//           emptySearchResults();
//           return; // Exit the function early if the search term is empty
//         }
    
//         var request = new XMLHttpRequest();
//         var requestUrl =
//           '/_hcms/search?&term=' +
//           encodeURIComponent(searchTerm) +
//           '&limit=' +
//           encodeURIComponent(TYPEAHEAD_LIMIT) +
//           `&autocomplete=true&analytics=true` +
//           searchOptions();
  
//         request.open('GET', requestUrl, true);
//         request.onload = function () {
//           if (request.status >= 200 && request.status < 400) {
//             var data = JSON.parse(request.responseText);
//             if (data.total > 0) {
//               fillSearchResults(data);
//               trapFocus();
//             } else {
//               emptySearchResults();
//             }
//           } else {
//             console.error('Server reached, error retrieving results.');
//           }
//         };
//         request.onerror = function () {
//           console.error('Could not reach the server.');
//         };
//         request.send();
//       },
//       trapFocus = function () {
//         var tabbable = [];
//         tabbable.push(searchField);
//         var tabbables = searchResults.getElementsByTagName('A');
//         for (var i = 0; i < tabbables.length; i++) {
//           tabbable.push(tabbables[i]);
//         }
//         var firstTabbable = tabbable[0],
//           lastTabbable = tabbable[tabbable.length - 1];
//         var tabResult = function (e) {
//           if (e.target == lastTabbable && !e.shiftKey) {
//             e.preventDefault();
//             firstTabbable.focus();
//           } else if (e.target == firstTabbable && e.shiftKey) {
//             e.preventDefault();
//             lastTabbable.focus();
//           }
//         },
//           nextResult = function (e) {
//             e.preventDefault();
//             if (e.target == lastTabbable) {
//               firstTabbable.focus();
//             } else {
//               tabbable.forEach(function (el) {
//                 if (el == e.target) {
//                   tabbable[tabbable.indexOf(el) + 1].focus();
//                 }
//               });
//             }
//           },
//           lastResult = function (e) {
//             e.preventDefault();
//             if (e.target == firstTabbable) {
//               lastTabbable.focus();
//             } else {
//               tabbable.forEach(function (el) {
//                 if (el == e.target) {
//                   tabbable[tabbable.indexOf(el) - 1].focus();
//                 }
//               });
//             }
//           };
//         searchForm.addEventListener('keydown', function (e) {
//           switch (e.key) {
//             case KEYS.TAB:
//               tabResult(e);
//               break;
//             case KEYS.ESC:
//             case KEYS.ESCAPE:
//               emptySearchResults();
//               break;
//             case KEYS.UP:
//             case KEYS.ARROW_UP:
//               lastResult(e);
//               break;
//             case KEYS.DOWN:
//             case KEYS.ARROW_DOWN:
//               nextResult(e);
//               break;
//           }
//         });
//       },
//       isSearchTermPresent = function () {
//         searchTerm = searchField.value;
//         if (searchTerm.length === 0) {
//           searchIcon.src = "https://5887777.fs1.hubspotusercontent-na1.net/hubfs/5887777/search.svg";
//         } else {
//           getSearchResults();
//           searchIcon.src = "https://7222284.fs1.hubspotusercontent-na1.net/hubfs/7222284/x-circle.svg";
//         }
//       };
    
//       var init = (function () {
//         searchField.addEventListener('input', function (e) {
//           isSearchTermPresent();
//         });
//         searchIcon.addEventListener("click", function () {
//           if (searchIcon.src == "https://7222284.fs1.hubspotusercontent-na1.net/hubfs/7222284/x-circle.svg") {
//             searchField.value = "";
//             searchIcon.src = "https://5887777.fs1.hubspotusercontent-na1.net/hubfs/5887777/search.svg";
//           }
//         });
//       })();
//   };
  

//   document.getElementById("searchInput").addEventListener("focus", function () {
//     document.getElementById("searchContainer").classList.add("focused");
//   });
//   document.getElementById("searchInput").addEventListener("blur", function () {
//     document.getElementById("searchContainer").classList.remove("focused");
//   });

//   if (
//     document.attachEvent
//       ? document.readyState === 'complete'
//       : document.readyState !== 'loading'
//   ) {
//     var searchResults = document.querySelectorAll('.hs-search-field');
//     Array.prototype.forEach.call(searchResults, function (el) {
//       var hsSearchModule = hsSearch(el);
//     });
//   } else {
//     document.addEventListener('DOMContentLoaded', function () {
//       var searchResults = document.querySelectorAll('.hs-search-field');
//       Array.prototype.forEach.call(searchResults, function (el) {
//         var hsSearchModule = hsSearch(el);
//       });
//     });
//   };

// });

// // https://hubspot-demo-account-niswey-7222284.hs-sites.com/atri-theme
// // hubspot search 

