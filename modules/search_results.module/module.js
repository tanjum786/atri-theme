document.addEventListener("DOMContentLoaded", function () {
  let searchIcon = document.querySelector(".search-icon");
  searchIcon.addEventListener("click", function () {
    // Redirect to the listing page of the previous page
    window.history.back();
  });

  var hsResultsPage = function (_resultsClass) {
    function buildResultsPage(_instance) {
      var resultTemplate = _instance.querySelector(
        '.hs-search-results__template'
      );
      var resultsSection = _instance.querySelector('.hs-search-results__listing');
      var resultsSection1 = _instance.querySelector('.error');

      var searchPath = _instance
        .querySelector('.hs-search-results__pagination')
        .getAttribute('data-search-path');
      var prevLink = _instance.querySelector('.hs-search-results__prev-page');
      var nextLink = _instance.querySelector('.hs-search-results__next-page');

      var searchParams = new URLSearchParams(window.location.search.slice(1));

      /**
       * v1 of the search input module uses the `q` param for the search query.
       * This check is a fallback for a mixed v0 of search results and v1 of search input.
       */

      if (searchParams.has('q')) {
        searchParams.set('term', searchParams.get('q'));
        searchParams.delete('q');
      }

      function getTerm() {
        return searchParams.get('term') || '';
      }
      function getOffset() {
        return parseInt(searchParams.get('offset')) || 0;
      }
      function getLimit() {
        return parseInt(searchParams.get('limit'));
      }
      function addResult(title, url, description, featuredImage) {
        var newResult = document.importNode(resultTemplate.content, true);
        function isFeaturedImageEnabled() {
          if (
            newResult.querySelector('.hs-search-results__featured-image > img')
          ) {
            return true;
          }
        }
        newResult.querySelector('.hs-search-results__title h3').innerHTML = title;

        newResult.querySelector('.hs-search-results__title').href = url;
        if (typeof featuredImage !== 'undefined' && isFeaturedImageEnabled()) {
          newResult.querySelector(
            '.hs-search-results__featured-image > img'
          ).src = featuredImage;
        }
        resultsSection.appendChild(newResult);
      }
      function fillResults(results) {
        var searchTerm = getTerm().toLowerCase();
        if (searchTerm.length > 0) {
          searchIcon.src = "https://7222284.fs1.hubspotusercontent-na1.net/hubfs/7222284/x-circle.svg";
        } else {
          searchIcon.src = "https://5887777.fs1.hubspotusercontent-na1.net/hubfs/5887777/search.svg";
          window.history.back();
        }
        var filteredResults = results.results.filter(function (result) {
          return result.title.toLowerCase().includes(searchTerm);
        });
        console.log(filteredResults);
        if (filteredResults.length > 0) {
          filteredResults.forEach(function (result, i) {
            addResult(
              result.title,
              result.url,
              result.description,
              result.featuredImageUrl
            );
          });
          paginate(filteredResults); // Paginate the filtered results
        } else {
          emptyResults(results.searchTerm); // If no matching results, display the empty results message
          emptyPagination();
        }
      }


      function emptyPagination() {
        prevLink.innerHTML = '';
        nextLink.innerHTML = '';
      }
      function emptyResults(searchedTerm) {
        if (searchedTerm.length > 0) {
          searchIcon.src = "https://7222284.fs1.hubspotusercontent-na1.net/hubfs/7222284/x-circle.svg";
        } else {
          searchIcon.src = "https://5887777.fs1.hubspotusercontent-na1.net/hubfs/5887777/search.svg";
          window.history.back();
        }
        resultsSection1.innerHTML =
          '<div class="hs-search__no-results no-result-container-temp"><span class="no-result">Oops! Looks like there are no posts related to "' +
          searchedTerm +
          '"</span>'
      }
      function setSearchBarDefault(searchedTerm) {
        var searchBars = document.querySelectorAll('.hs-search-field__input');
        Array.prototype.forEach.call(searchBars, function (el) {
          el.value = searchedTerm;
        });
      }
      function httpRequest(term, offset) {
        console.log('httpRequest')
        var SEARCH_URL = '/_hcms/search?';
        var requestUrl = SEARCH_URL + searchParams + '&analytics=true&groupId=163246884742';
        var request = new XMLHttpRequest();
        request.open('GET', requestUrl, true);
        request.onload = function () {
          if (request.status >= 200 && request.status < 400) {
            var data = JSON.parse(request.responseText);
            setSearchBarDefault(data.searchTerm);
            if (data.total > 0) {
              fillResults(data);
              paginate(data);
            } else {
              searchIcon.src = "https://7222284.fs1.hubspotusercontent-na1.net/hubfs/7222284/x-circle.svg";
              emptyResults(data.searchTerm);
              emptyPagination();
            }
          } else {
            console.error('Server reached, error retrieving results.');
          }
        };
        request.onerror = function () {
          console.error('Could not reach the server.');
        };
        request.send();
      }
      function paginate(results) {
        var updatedLimit = getLimit() || results.limit;

        function hasPreviousPage() {
          return results.page > 0;
        }
        function hasNextPage() {
          return results.offset <= results.total - updatedLimit;
        }

        if (hasPreviousPage()) {
          var prevParams = new URLSearchParams(searchParams.toString());
          prevParams.set(
            'offset',
            results.page * updatedLimit - parseInt(updatedLimit)
          );
          prevLink.href = searchPath + '?' + prevParams;
          prevLink.innerHTML = '&lt; Previous page';
        } else {
          prevLink.parentNode.removeChild(prevLink);
        }

        if (hasNextPage()) {
          var nextParams = new URLSearchParams(searchParams.toString());
          nextParams.set(
            'offset',
            results.page * updatedLimit + parseInt(updatedLimit)
          );
          nextLink.href = searchPath + '?' + nextParams;
          nextLink.innerHTML = 'Next page &gt;';
        } else {
          nextLink.parentNode.removeChild(nextLink);
        }
      }
      var getResults = (function () {
        if (getTerm()) {
          httpRequest(getTerm(), getOffset());
        } else {
          emptyPagination();
        }
      })();
    }
    (function () {
      var searchResults = document.querySelectorAll(_resultsClass);
      Array.prototype.forEach.call(searchResults, function (el) {
        buildResultsPage(el);
      });
    })();
  };

  if (
    document.attachEvent
      ? document.readyState === 'complete'
      : document.readyState !== 'loading'
  ) {
    var resultsPages = hsResultsPage('div.hs-search-results');
  } else {
    document.addEventListener('DOMContentLoaded', function () {
      var resultsPages = hsResultsPage('div.hs-search-results');
    });
  }
});