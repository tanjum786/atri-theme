document.addEventListener("DOMContentLoaded", function () {
  let dropDownIcon = document.querySelector(".dropdown-icon img");
  let dropDownIconTopic = document.querySelector(".dropdown-icon-topic img");
  const filterButtons1 = document.querySelectorAll(
    ".blog-topic-filter .select-dropdown__list-item"
  );
  let searchInput = document.getElementById("searchInput");


  function getTagFromURLPath() {
    var url = window.location.href;
    var parts = url.split("/");
    var tagIndex = parts.indexOf("tag");
    if (tagIndex !== -1 && tagIndex < parts.length - 1) {
      return parts[tagIndex + 1];
    }
    return "";
  }

  // Update the button text based on tag from URL path
  var urlSelectedTag = getTagFromURLPath();

  if (urlSelectedTag) {

    document.getElementById("selectedTagButton").querySelector("span").innerText = urlSelectedTag;
    filterButtons1.forEach((button) => {
      let selectedTag = button.getAttribute("data-value").toLowerCase().replace(/\s+/g, '-');
      if (selectedTag === urlSelectedTag) {
        button.classList.add("selected");
      } else {
        button.classList.remove("selected");

      }
    });
  }

  filterButtons1.forEach((button) => {
    button.addEventListener("click", function () {
      const clickedTag = this.getAttribute("data-value").toLowerCase().replace(/\s+/g, '-');

      if (this.classList.contains("selected")) {
        window.location.href = "https://hubspot-demo-account-niswey-7222284.hs-sites.com/atri-theme/";
        this.classList.remove("selected");
      } else {
        window.location.href = "https://hubspot-demo-account-niswey-7222284.hs-sites.com/atri-theme/tag/" + clickedTag;
        filterButtons1.forEach((otherButton) => {
          if (otherButton !== this && otherButton.classList.contains("selected")) {
            otherButton.classList.remove("selected");
          }
        });
        this.classList.add("selected");
      }
    });
  });



  // Dropdown 1
  const button1 = document.querySelector(
    ".blog-topic-filter .select-dropdown__button"
  );
  const list1 = document.querySelector(
    ".blog-topic-filter .select-dropdown__list"
  );
  function toggleDropdownTopic() {
    if (list1.classList.contains("active")) {
      list1.classList.remove("active");
      button1.classList.remove("active-btn");
      dropDownIconTopic.classList.remove("rotate-icon");
      dropDownIconTopic.src =
        "https://5887777.fs1.hubspotusercontent-na1.net/hubfs/5887777/Icon.svg";
    } else {
      list1.classList.add("active");
      button1.classList.add("active-btn");
      dropDownIconTopic.classList.add("rotate-icon");
      dropDownIconTopic.src =
        "https://7222284.fs1.hubspotusercontent-na1.net/hubfs/7222284/chevron-down.svg";
    }
  }

  button1.addEventListener("click", function () {
    toggleDropdownTopic();

  });

  const blogs = document.querySelectorAll(".blog-list_wrapper");


  // Dropdown 2
  const button2 = document.querySelector(
    ".blog-type-filter .select-dropdown__button"
  );
  const list2 = document.querySelector(
    ".blog-type-filter .select-dropdown__list"
  );
  const blogs2 = document.querySelectorAll(".blog-list_wrapper");

  function toggleDropdown() {
    if (list2.classList.contains("active")) {
      list2.classList.remove("active");
      button2.classList.remove("active-btn");
      dropDownIcon.classList.remove("rotate-icon");
      dropDownIcon.src =
        "https://5887777.fs1.hubspotusercontent-na1.net/hubfs/5887777/Icon.svg";
    } else {
      list2.classList.add("active");
      button2.classList.add("active-btn");
      dropDownIcon.classList.add("rotate-icon");
      dropDownIcon.src =
        "https://7222284.fs1.hubspotusercontent-na1.net/hubfs/7222284/chevron-down.svg";
    }
  }

  button2.addEventListener("click", function () {
    toggleDropdown();

  });


  const filterButtons2 = document.querySelectorAll(
    ".blog-type-filter .select-dropdown__list-item"
  );

  filterButtons2.forEach((button) => {
    button.addEventListener("click", function () {
      const clickedType = this.getAttribute("data-value").toLowerCase().replace(/\s+/g, '-');

      if (this.classList.contains("selected")) {
        this.classList.remove("selected");
      } else {
        filterButtons2.forEach((otherButton) => {
          if (otherButton !== this && otherButton.classList.contains("selected")) {
            otherButton.classList.remove("selected");
          }
        });
        this.classList.add("selected");
      }
      handleClick(clickedType);
      handleReset(clickedType);
      console.log("click");
    });
  });



  let handleClick = (type) => {
    // Get the current URL
    let currentUrl = window.location.href;


    let updatedUrl;

    // Remove existing 'type' query parameter if it exists
    updatedUrl = currentUrl.replace(/([&?])type=[^&]+/, '');

    if (updatedUrl.includes('?')) {
      updatedUrl += `&type=${type}`;
    } else {
      updatedUrl += `?type=${type}`;
    }

    // Redirect to the updated URL
    window.location.href = updatedUrl; 
  };

  handleReset = (type) => {
    let newCurrentUrl = new URL(window.location.href);

    // Get the value of the 'type' query parameter
    let typeValue = newCurrentUrl.searchParams.get('type');
    console.log(typeValue);
    // Get the current URL
    let currentUrl = window.location.href;

    if (currentUrl.includes('?') && typeValue===type) {
      let updatedUrl = currentUrl.replace(/([&?])type=[^&]+/, '');

      window.location.assign(updatedUrl);
    }
  };


  document.addEventListener("DOMContentLoaded", function () {
    var selectedTag = getUrlParameter("tag");
    updateFilteredButton(selectedTag);
  });


  // Close dropdowns when clicking outside
  document.addEventListener("click", function (event) {
    const dropdownButtons = document.querySelectorAll(
      ".select-dropdown__button"
    );
    dropdownButtons.forEach((button) => {
      if (!button.contains(event.target)) {
        const dropdownList = button.nextElementSibling;
        dropdownList.classList.remove("active");
        const dropdownIcon = button.querySelector(".dropdown-icon img");
        let dropDownIconTopic = button.querySelector(
          ".dropdown-icon-topic img"
        );

        if (dropdownIcon) {
          dropdownIcon.classList.remove("rotate-icon");
        }
        if (dropDownIconTopic) {
          dropDownIconTopic.classList.remove("rotate-icon");
        }
      }
    });
  });

  // mobile view search  and filter functionality **************************************************
  let modal = document.querySelector(".search-modal");
  let closeBtn = document.querySelector(".close-icon");
  let errorMsg = document.querySelector(".error-msg");
  let searchBtn = document.querySelector(".modal-searchBtn");
  function toggleSearchModal() {
    modal.classList.toggle("modal-toggle");
    document.body.classList.toggle("modal-open"); // Add or remove class to body
  }

  // mobileSearchResultBtn.addEventListener("click", function () {
  //   toggleSearchModal();
  //   searchBtn.classList.add("btn-bg");
  // });

  // Event listeners for opening and closing search modal
  searchBtn.addEventListener("click", toggleSearchModal);
  closeBtn.addEventListener("click", function () {
    toggleSearchModal();
    document.getElementById("mobile-searchContainer").classList.remove("focused");
  });

  searchInput.addEventListener("focus", function () {
    document.getElementById("mobile-searchContainer").classList.add("focused");
  });

  // filter modal 
  const mobileFilterBtn = document.querySelector(".modal-filter_btn");
  let filterModal = document.querySelector(".filter-modal");
  let filterBtn = document.querySelector(".modal-filterBtn");
  let filterCloseBtn = document.querySelector(".filter-close-icon");
  let selectedTopicItems = document.querySelectorAll(".blog-modal_topic-list .modal-select-dropdown__list-item");
  let selectedTypeItems = document.querySelectorAll(".blog-modal_type-list .modal-select-dropdown__list-item-type");


  selectedTopicItems.forEach((button) => {
    button.addEventListener("click", function () {
      const clickedTag = this.getAttribute("data-value").toLowerCase().replace(/\s+/g, '-');

      if (this.classList.contains("selected")) {
        window.location.href = "https://hubspot-demo-account-niswey-7222284.hs-sites.com/atri-theme/";
        this.classList.remove("selected");
      } else {
        window.location.href = "https://hubspot-demo-account-niswey-7222284.hs-sites.com/atri-theme/tag/" + clickedTag;
        filterButtons1.forEach((otherButton) => {
          if (otherButton !== this && otherButton.classList.contains("selected")) {
            otherButton.classList.remove("selected");
          }
        });
        this.classList.add("selected");
      }
    });
  });
  if (urlSelectedTag) {

    selectedTopicItems.forEach((button) => {
      let selectedTag = button.getAttribute("data-value").toLowerCase().replace(/\s+/g, '-');
      if (selectedTag === urlSelectedTag) {
        button.classList.add("selected");
      } else {
        button.classList.remove("selected");

      }
    });
  }


  function toggleFilterModal() {
    filterModal.classList.toggle("filter-modal_toggle");
    document.body.classList.toggle("modal-open"); // Add or remove class to body

  }
  // Event listeners for opening and closing filter modal
  filterBtn.addEventListener("click", toggleFilterModal);
  filterCloseBtn.addEventListener("click", toggleFilterModal);



  // hubsot search

  var hsSearch = function (_instance) {
    var TYPEAHEAD_LIMIT = 3;
    var KEYS = {
      TAB: 'Tab',
      ESC: 'Esc', // IE11 & Edge 16 value for Escape
      ESCAPE: 'Escape',
      UP: 'Up', // IE11 & Edge 16 value for Arrow Up
      ARROW_UP: 'ArrowUp',
      DOWN: 'Down', // IE11 & Edge 16 value for Arrow Down
      ARROW_DOWN: 'ArrowDown',
    };
    var searchTerm = '',
      searchForm = _instance,
      searchField = _instance.querySelector('.hs-search-field__input'),
      searchResults = _instance.querySelector('.hs-search-field__suggestions'),
      searchIcon = _instance.querySelector('.search-icon'),
      mobileSearchIcon = _instance.querySelector('.modal-search-icon'),

      searchOptions = function () {
        var formParams = [];
        var form = _instance.querySelector('form');
        for (
          var i = 0;
          i < form.querySelectorAll('input[type=hidden]').length;
          i++
        ) {
          var e = form.querySelectorAll('input[type=hidden]')[i];
          if (e.name !== 'limit') {
            formParams.push(
              encodeURIComponent(e.name) + '=' + encodeURIComponent(e.value)
            );
          }
        }
        var queryString = formParams.join('&');
        return queryString;
      };

    var debounce = function (func, wait, immediate) {
      var timeout; searchForm
      return function () {
        var context = this,
          args = arguments;
        var later = function () {
          timeout = null;
          if (!immediate) {
            func.apply(context, args);
          }
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait || 200);
        if (callNow) {
          func.apply(context, args);
        }
      };
    },
      emptySearchResults = function () {
        searchResults.innerHTML = '';
        searchField.focus();
        searchForm.classList.remove('hs-search-field--open');
      },
      fillSearchResults = function (response) {
        var items = [];
        items.push(
          "<li id='results-for'>Results for \"" + response.searchTerm + '"</li>'
        );
        response.results.forEach(function (val, index) {
          items.push(
            "<li id='result" +
            index +
            "'><a href='" +
            val.url +
            "'>" +
            val.title +
            '</a></li>'
          );
          items.push("<li>Post Type: " + val.blog_resource_type + "</li>");

        });

        emptySearchResults();
        searchResults.innerHTML = items.join('');
        searchForm.classList.add('hs-search-field--open');
      },


      getSearchResults = function () {
        if (searchTerm.trim().length === 0) {
          // Clear the search results if the search term is empty
          emptySearchResults();
          return; // Exit the function early if the search term is empty
        }

        var request = new XMLHttpRequest();
        var requestUrl =
          '/_hcms/search?&term=' +
          encodeURIComponent(searchTerm) +
          '&limit=' +
          encodeURIComponent(TYPEAHEAD_LIMIT) +
          `&autocomplete=true&analytics=true` +
          searchOptions();

        request.open('GET', requestUrl, true);
        request.onload = function () {
          if (request.status >= 200 && request.status < 400) {
            var data = JSON.parse(request.responseText);
            if (data.total > 0) {
              fillSearchResults(data);
              trapFocus();
            } else {
              emptySearchResults();
            }
          } else {
            console.error('Server reached, error retrieving results.');
          }
        };
        request.onerror = function () {
          console.error('Could not reach the server.');
        };
        request.send();
      },
      trapFocus = function () {
        var tabbable = [];
        tabbable.push(searchField);
        var tabbables = searchResults.getElementsByTagName('A');
        for (var i = 0; i < tabbables.length; i++) {
          tabbable.push(tabbables[i]);
        }
        var firstTabbable = tabbable[0],
          lastTabbable = tabbable[tabbable.length - 1];
        var tabResult = function (e) {
          if (e.target == lastTabbable && !e.shiftKey) {
            e.preventDefault();
            firstTabbable.focus();
          } else if (e.target == firstTabbable && e.shiftKey) {
            e.preventDefault();
            lastTabbable.focus();
          }
        },
          nextResult = function (e) {
            e.preventDefault();
            if (e.target == lastTabbable) {
              firstTabbable.focus();
            } else {
              tabbable.forEach(function (el) {
                if (el == e.target) {
                  tabbable[tabbable.indexOf(el) + 1].focus();
                }
              });
            }
          },
          lastResult = function (e) {
            e.preventDefault();
            if (e.target == firstTabbable) {
              lastTabbable.focus();
            } else {
              tabbable.forEach(function (el) {
                if (el == e.target) {
                  tabbable[tabbable.indexOf(el) - 1].focus();
                }
              });
            }
          };
        searchForm.addEventListener('keydown', function (e) {
          switch (e.key) {
            case KEYS.TAB:
              tabResult(e);
              break;
            case KEYS.ESC:
            case KEYS.ESCAPE:
              emptySearchResults();
              break;
            case KEYS.UP:
            case KEYS.ARROW_UP:
              lastResult(e);
              break;
            case KEYS.DOWN:
            case KEYS.ARROW_DOWN:
              nextResult(e);
              break;
          }
        });
      },
      isSearchTermPresent = function () {
        searchTerm = searchField.value;
        if (searchTerm.length === 0) {
          searchIcon.src = "https://5887777.fs1.hubspotusercontent-na1.net/hubfs/5887777/search.svg";
        } else {
          getSearchResults();
          console.log(searchTerm, "else========");
          searchIcon.src = "https://7222284.fs1.hubspotusercontent-na1.net/hubfs/7222284/x-circle.svg";
        }
      };
    (function () {
      searchField.addEventListener('input', function (e) {
        isSearchTermPresent();
        console.log("callfunction");
      });
      searchIcon.addEventListener("click", function () {
        if (searchIcon.src == "https://7222284.fs1.hubspotusercontent-na1.net/hubfs/7222284/x-circle.svg") {
          searchField.value = "";
          searchIcon.src = "https://5887777.fs1.hubspotusercontent-na1.net/hubfs/5887777/search.svg";
        }
      });
    })();
  };


  document.getElementById("searchInput").addEventListener("focus", function () {
    document.getElementById("searchContainer").classList.add("focused");
  });
  document.getElementById("searchInput").addEventListener("blur", function () {
    document.getElementById("searchContainer").classList.remove("focused");
  });

  if (
    document.attachEvent
      ? document.readyState === 'complete'
      : document.readyState !== 'loading'
  ) {
    var searchResults = document.querySelectorAll('.hs-search-field');
    Array.prototype.forEach.call(searchResults, function (el) {
      var hsSearchModule = hsSearch(el);
    });
  } else {
    document.addEventListener('DOMContentLoaded', function () {
      var searchResults = document.querySelectorAll('.hs-search-field');
      Array.prototype.forEach.call(searchResults, function (el) {
        var hsSearchModule = hsSearch(el);
      });
    });
  };

});

// https://hubspot-demo-account-niswey-7222284.hs-sites.com/atri-theme
// hubspot search 

