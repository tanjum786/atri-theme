document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("play-button").addEventListener("click", function() {
        var thumbnailOverlay = document.querySelector('.thumbnail-overlay');
        var videoIframe = document.getElementById('video-iframe');

        // Hide the thumbnail overlay
        thumbnailOverlay.style.display = 'none';

        // Start playing the video with autoplay
        var src = videoIframe.src;
        videoIframe.src = src + (src.includes('?') ? '&' : '?') + 'autoplay=1';
    });
});
